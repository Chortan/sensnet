#include <DHTesp.h>

class DHTSensor {
  private:
    DHTesp dht;
  public:
    DHTSensor(uint8_t pin, DHTesp::DHT_MODEL_t type) {
       this->dht.setup(pin, type);
    }

    float getTemperature() {
      return dht.getTemperature();
    }

    float getHumidity() {
      return dht.getHumidity();
    }

    String toJson() {
      float humidity = this->getHumidity();;
      float temperature = this->getTemperature();
      return "{\"temperature\": " + (isnan(temperature) ? "null" : String(temperature)) + ", \"humidity\": " +  (isnan(humidity) ? "null" : String(humidity)) + "}"; 
    }
};
