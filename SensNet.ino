#include <DHTesp.h>
#include <ESP8266WebServer.h>
#include "DHTSensor.h"
#include "WifiManager.h"
#include "WebTemplate.h"

#define DHTPin D5

DHTSensor dht(DHTPin, DHTesp::AM2302);
ESP8266WebServer server(80);
WifiManager* manager;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  manager = new WifiManager;
  server.on("/", setupPage);
  server.on("/configure", setupPostPage);
  server.begin();

}

void loop() {
  // put your main code here, to run repeatedly:
  server.handleClient();
}



void setupPage() {
  server.send(200, "text/html", manager->setupTemplate());
}

void setupPostPage() {
  if(server.args() == 2) {
    String ssid, password;
    for(int i=0; i<server.args(); i++) {
      switch(server.argName(i)) {
        case "ssid": ssid = server.arg(i); break
        case "password": password = server.arg(i); break;
        default: badRequest();
      }
    }
    manager->setupClient(ssid, password);
  }
}

void badRequest() {
  server.send(400, "text/html", template_error("Bad request"));
}
