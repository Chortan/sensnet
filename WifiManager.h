#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include "WebTemplate.h"
#include <ESP8266WebServer.h>
#include <string.h>

#define APSSID "SensNet"
#define APPASS "SensNet1234"
#define APCHAN 1
#define APHIDD false
#define APMAXC 1
#define APIP {"192","168","1","1"}
#define APGW "192.168.1.254"
#define APSN "255.255.255.0"

class WifiManager{
  private:
    String ssid;
    String password;
    uint8_t networkAvailable;
    char **ssids;
    const char **rssis;
  public: 
   void setupAP() {
      Serial.println("Setup AP");
      WiFi.softAP(APSSID, APPASS, APCHAN, APHIDD, APMAXC);
      IPAddress myIP = WiFi.softAPIP();
      Serial.println(myIP);
    }
    
    WifiManager() {
      this->scan();
      this->setupAP();
    }

    WifiManager(String ssid, String password) {
      this->ssid = ssid;
      this->password = password;
      this->setupClient(ssid, password);
    }

    void scan() {
      free(this->ssids);
      WiFi.mode(WIFI_STA);
      WiFi.disconnect();
      this->networkAvailable = WiFi.scanNetworks();
      this->ssids= (char **)malloc(sizeof(char) * this->networkAvailable);
      this->rssis= (const char **)malloc(sizeof(const char) * this->networkAvailable);
      for(int i=0;i<this->networkAvailable; i++) {
        const char *ssid = WiFi.SSID(i).c_str();
        this->ssids[i] = (char*)malloc(strlen(ssid)+1);
        strcpy(this->ssids[i], ssid);
       // this->ssids[i] = ssid;
        Serial.println(this->ssids[i]);
        
        //this->rssis[i] = WiFi.RSSI(i).c_str();
      }
      
    }

    

    void setupClient(String ssid, String password) {
      this->ssid = ssid;
      this->password = password;
      WiFi.disconnect();
      WiFi.mode(WIFI_STA);
      WiFi.begin(ssid, password);
        
       while (WiFi.status() != WL_CONNECTED) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(250);
        digitalWrite(LED_BUILTIN, LOW);
        delay(250);
      }
      
    }

    bool isConfigured() {
        return false;
//      return !isnan(this->ssid) && !isnan(this->password);
    }

    String setupTemplate() {
      String playload = 
        "<h1>SensNet</h1>"+
        String("<p>Connect your Home</p>")+
        String("<form action=\"/configure\" method=\"POST\">")+
          String("<label for=\"ssid\">SSID</label>")+
          String("<select name=\"ssid\" id=\"ssid\" type=\"text\">");

      for(int i=0; i<this->networkAvailable; i++) {
        playload = playload + 
          String("<option value=\"" + 
            String(WiFi.SSID(i)) + "\"> " + 
            String(WiFi.SSID(i)) + 
          "</option>");
      }

      playload = playload +
          String("</select>") +
          String("<label for=\"password\">Mot de passe</label>")+
          String("<input name=\"password\" id=\"password\" type=\"password\"/>")+
          String("<button type=\"submit\">Configurer</button>")+
        String("</form>")
      ;
      
      return template_default(playload);
    }

    

    
    void setPassword(String password) {
      this->password = password;
    }

    void setSsid(String ssid) {
      this->ssid = ssid;
    }

    String getSsid() {
      return this->ssid;
    }

    
  
};
